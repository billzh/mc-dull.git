﻿<template>
  <page-header-wrapper>
    <a-card :bordered="false">
      <!-- 条件搜索 -->
      <div class="table-page-search-wrapper">
        <a-form layout="inline">
          <a-row :gutter="48">
            ${vueQueryFormHtml}
          <a-col :md="!advanced && 8 || 24" :sm="24">
            <span class="table-page-search-submitButtons" :style="advanced && { float: 'right', overflow: 'hidden' } || {} ">
              <a-button type="primary" @click="handleQuery"><a-icon type="search" />查询</a-button>
              <a-button style="margin-left: 8px" @click="resetQuery"><a-icon type="redo" />重置</a-button>
              <a @click="toggleAdvanced" style="margin-left: 8px">{{ advanced ? '收起' : '展开' }}<a-icon :type="advanced ? 'up' : 'down'"/></a>
            </span>
          </a-col>
          </a-row>
        </a-form>
      </div>
      <!-- 操作 -->
      <div class="table-operations">
        <a-button type="primary" @click="${refs}refs.createForm.handleAdd()" v-hasPermi="['${replaceDto.PermissionPrefix}:add']">
          <a-icon type="plus" />新增
        </a-button>
        <a-button type="primary" :disabled="single" @click="${refs}refs.createForm.handleUpdate(undefined, ids)" v-hasPermi="['${replaceDto.PermissionPrefix}:edit']">
          <a-icon type="edit" />修改
        </a-button>
        <a-button type="danger" :disabled="multiple" @click="handleDelete" v-hasPermi="['${replaceDto.PermissionPrefix}:delete']">
          <a-icon type="delete" />删除
        </a-button>
        $if(replaceDto.ShowBtnExport)
        <a-button type="primary" @click="handleExport" v-hasPermi="['${refs}{replaceDto.PermissionPrefix}:export']">
          <a-icon type="download" />导出
        </a-button>
        $end
        <a-button
          type="dashed"
          shape="circle"
          :loading="loading"
          :style="{float: 'right'}"
          icon="reload"
          @click="getList" />
      </div>
      <!-- 增加修改 -->
      <create-form
        ref="createForm"
        @ok="getList"
      />
     <!-- 数据展示 -->
      <a-table
        :loading="loading"
        :size="tableSize"
        rowKey="noticeId"
        :columns="columns"
        :data-source="list"
        :row-selection="{ selectedRowKeys: selectedRowKeys, onChange: onSelectChange }"
        :pagination="false">
        <span slot="noticeType" slot-scope="text, record">
          {{ typeFormat(record) }}
        </span>
        <span slot="status" slot-scope="text, record">
          {{ statusFormat(record) }}
        </span>
        <span slot="createTime" slot-scope="text, record">
          {{ parseTime(record.createTime) }}
        </span>
        <span slot="operation" slot-scope="text, record">
          <a @click="${refs}refs.createForm.handleUpdate(record, undefined)" v-hasPermi="['system:notice:edit']">
            <a-icon type="edit" />修改
          </a>
          <a-divider type="vertical" v-hasPermi="['system:notice:remove']" />
          <a @click="handleDelete(record)" v-hasPermi="['system:notice:remove']">
            <a-icon type="delete" />删除
          </a>
        </span>
      </a-table>
      <!-- 分页 -->
      <a-pagination
        class="ant-table-pagination"
        show-size-changer
        show-quick-jumper
        :current="queryParam.pageNum"
        :total="total"
        :page-size="queryParam.pageSize"
        :showTotal="total"
        @showSizeChange="onShowSizeChange"
        @change="changeSize"
      />
    </a-card>
  </page-header-wrapper>
</template>

<script>

import { 
  list${genTable.BusinessName},
  add${genTable.BusinessName},
  del${genTable.BusinessName},
  update${genTable.BusinessName},
  get${genTable.BusinessName},
$if(replaceDto.ShowBtnExport)
  export${genTable.BusinessName},
$end
$if(showCustomInput)
  changeSort
$end
} from '@/api/${tool.FirstLowerCase(genTable.ModuleName)}/${tool.FirstLowerCase(genTable.BusinessName)}';

import CreateForm from './modules/CreateForm'
$if(replaceDto.ShowBtnExport)
 import { download } from '@/utils/request'
$end

export default {
  name: "${genTable.BusinessName.ToLower()}",
  components: {
    CreateForm
  },
  data () {
    return {
      list: [],
      selectedRowKeys: [],
      selectedRows: [],
      // 高级搜索 展开/关闭
      advanced: false,
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      ids: [],
      loading: false,
      total: 0,
      // 查询参数
      queryParam: {
        pageNum: 1,
        pageSize: 10,
         sort: undefined,
        sortType: undefined,
$foreach(item in genTable.Columns)
$if(item.IsQuery == true)
        ${item.CsharpFieldFl}: undefined,
$end
$end
      },
        columns: [
$set(index = 0)
$foreach(column in genTable.Columns)
        {  dataIndex: '${column.CsharpFieldFl}',ellipsis: true, title: `${column.ColumnComment}`, checked: $if(index < 9) true $else false $end },
$set(index = index + 1)
$end
        {
          title: '操作',
          dataIndex: 'operation',
          width: '15%',
          scopedSlots: { customRender: 'operation' },
          align: 'center'
        }
      ]
    }
  },
  filters: {
  },
  created () {
    this.getList()
  },
  computed: {
  },
  watch: {
  },
  methods: {
    /** 查询【请填写功能名称】列表 */
    getList () {
      this.loading = true
      list${genTable.BusinessName}(this.queryParam).then(response => {
        this.list = response.data.result
        this.total = response.data.totalNum
        this.loading = false
      })
    },
    /** 搜索按钮操作 */
    handleQuery () {
      this.queryParam.pageNum = 1
      this.getList()
    },
    /** 重置按钮操作 */
    resetQuery () {
      this.queryParam = {
 $foreach(item in genTable.Columns)
$if(item.HtmlType == "datetime" && item.IsQuery == true)
      //${item.ColumnComment}时间范围
      item.CsharpField:undefined,
$end
$end
        pageNum: 1,
        pageSize: 10
      }
      this.handleQuery()
    },
    onShowSizeChange (current, pageSize) {
      this.queryParam.pageSize = pageSize
      this.getList()
    },
    changeSize (current, pageSize) {
      this.queryParam.pageNum = current
      this.queryParam.pageSize = pageSize
      this.getList()
    },
    onSelectChange (selectedRowKeys, selectedRows) {
      this.selectedRowKeys = selectedRowKeys
      this.selectedRows = selectedRows
      this.ids = this.selectedRows.map(item => item.${replaceDto.FistLowerPk})
      this.single = selectedRowKeys.length !== 1
      this.multiple = !selectedRowKeys.length
    },
    toggleAdvanced () {
      this.advanced = !this.advanced
    },
    /** 删除按钮操作 */
    handleDelete (row) {
      var that = this
      const Ids = row.${replaceDto.FistLowerPk} || this.ids
      this.${confirm}confirm({
        title: '确认删除所选中数据?',
        content: '当前选中编号为' + Ids + '的数据',
        onOk () {
          return del${genTable.BusinessName}(Ids)
            .then(() => {
              that.onSelectChange([], [])
              that.getList()
              that.message.success(
                '删除成功',
                3
              )
          })
        },
        onCancel () {}
      })
    },
    /** 导出按钮操作 */
    handleExport () {
      var that = this
      this.${confirm}confirm({
        title: '是否确认导出?',
        content: '此操作将导出当前条件下所有数据而非选中数据',
        onOk () {
          export${genTable.BusinessName}(queryParams).then((response) => {
            const { code, data } = response
            if (code == 200) {
              that.message.success('导出成功', 3)
              download(data.path)
            } else {
              that.message.success('导出失败', 3)
            }
          }) 
        },
        onCancel () {}
      })
    }
  }
}
</script>
