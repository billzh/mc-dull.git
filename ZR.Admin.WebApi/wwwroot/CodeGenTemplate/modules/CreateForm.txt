<template>
  <a-drawer width="35%" :label-col="4" :wrapper-col="14" :visible="open" @close="onClose">
    <a-divider orientation="left">
      <b>{{ formTitle }}</b>
    </a-divider>
    <a-form-model ref="form" :model="form" :rules="rules">
      ${VueViewFormContent}
      <div class="bottom-control">
        <a-space>
          <a-button type="primary" @click="submitForm"> 保存 </a-button>
          <a-button type="dashed" @click="cancel"> 取消 </a-button>
        </a-space>
      </div>
    </a-form-model>
  </a-drawer>
</template>

<script>
import { 
get${genTable.BusinessName},
add${genTable.BusinessName},
update${genTable.BusinessName} }
from '@/api/${tool.FirstLowerCase(genTable.ModuleName)}/${tool.FirstLowerCase(genTable.BusinessName)}';


export default {
  name: 'CreateForm',
  props: {
    statusOptions: {
      type: Array,
      required: true,
    },
  },
  components: {},
  data() {
    return {
      loading: false,
      formTitle: '',
      // 表单参数
      form: {
      $foreach(item in genTable.Columns)
$if(item.IsQuery == true)
        ${item.CsharpFieldFl}: undefined,
$end
$end
      },
      open: false,
      rules: {
       $foreach(column in genTable.Columns)
$if(column.IsRequired && column.IsIncrement == false)
        ${column.CsharpFieldFl}: [
          { required: true, message: "${column.ColumnComment}不能为空", trigger: $if(column.htmlType == "select")"change"$else"blur"$end
$if(column.CsharpType == "int" || column.CsharpType == "long"), type: "number"$end }
        ],
$end
$end
      },
    }
  },
  filters: {},
  created() {},
  computed: {},
  watch: {},
  methods: {
    onClose() {
      this.open = false
    },
    // 取消按钮
    cancel() {
      this.open = false
      this.reset()
    },
    // 表单重置
    reset() {
      this.form = {
     $foreach(item in genTable.Columns)
$if((item.IsEdit || item.IsInsert))
        $item.CsharpFieldFl: undefined,
$end
$if((item.HtmlType == "checkbox"))
        ${item.CsharpFieldFl}Checked: [],
$end
$end
      }
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset()
      this.open = true
      this.formTitle = '添加岗位'
    },
    /** 修改按钮操作 */
    handleUpdate(row, ids) {
      this.reset()
      const id = row.${replaceDto.FistLowerPk} || this.ids;
      get${genTable.BusinessName}(id).then((response) => {
        this.form = response.data
        this.open = true
        this.formTitle = '修改岗位'
      })
    },
    /** 提交按钮 */
    submitForm: function () {
      this.${refs}refs.form.validate((valid) => {
        if (valid) {
$foreach(item in genTable.Columns)
$if(item.HtmlType == "checkbox")
          this.form.${item.CsharpFieldFl} = this.form.${item.CsharpFieldFl}Checked.toString();
$end
$end
          if (this.form.${replaceDto.FistLowerPk} !== undefined) {
            update${genTable.BusinessName}(this.form).then((response) => {
              this.message.success('修改成功', 3)
              this.open = false
              this.emit('ok')
            })
          } else {
            add${genTable.BusinessName}(this.form).then((response) => {
              this.open = false
              this.emit('ok')
            })
          }
        } else {
          return false
        }
      })
    },
  },
}
</script>
