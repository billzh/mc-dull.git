using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Attribute;
using Infrastructure.Enums;
using Infrastructure.Model;
using Mapster;
using ZR.Model.Dto;
using ZR.Model.Models;
using ZR.Service.Business.IBusinessService;
using ZR.Admin.WebApi.Extensions;
using ZR.Admin.WebApi.Filters;
using ZR.Common;
using Infrastructure.Extensions;
using System.Linq;

namespace ZR.Admin.WebApi.Controllers
{
    /// <summary>
    /// Controller
    ///
    /// @author zr
    /// @date 2022-03-30
    /// </summary>
    [Verify]
    [Route("book/BizBook")]
    public class BizBookController : BaseController
    {
        /// <summary>
        /// 接口
        /// </summary>
        private readonly IBizBookService _BizBookService;

        public BizBookController(IBizBookService BizBookService)
        {
            _BizBookService = BizBookService;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        [HttpGet("list")]
        [ActionPermissionFilter(Permission = "business:bizbook:list")]
        public IActionResult QueryBizBook([FromQuery] BizBookQueryDto parm)
        {
            var response = _BizBookService.GetList(parm);
            return SUCCESS(response);
        }


        /// <summary>
        /// 查询详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id}")]
        [ActionPermissionFilter(Permission = "business:bizbook:query")]
        public IActionResult GetBizBook(long Id)
        {
            var response = _BizBookService.GetFirst(x => x.Id == Id);

            return SUCCESS(response);
        }



        /// <summary>
        /// 添加图书管理
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionPermissionFilter(Permission = "book:bizbook:add")]
        [Log(Title = "图书管理", BusinessType = BusinessType.INSERT)]
        public IActionResult AddBizBook([FromBody] BizBookDto parm)
        {
            if (parm == null)
            {
                throw new CustomException("请求参数错误");
            }
            //从 Dto 映射到 实体
            var model = parm.Adapt<BizBook>().ToCreate(HttpContext);

            var response = _BizBookService.Insert(model, it => new
            {
                it.BookName,
                it.Price,
                it.Author,
                it.Content,
            });
            return ToResponse(response);
        }

        /// <summary>
        /// 更新图书管理
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ActionPermissionFilter(Permission = "book:bizbook:edit")]
        [Log(Title = "图书管理", BusinessType = BusinessType.UPDATE)]
        public IActionResult UpdateBizBook([FromBody] BizBookDto parm)
        {
            if (parm == null)
            {
                throw new CustomException("请求实体不能为空");
            }
            //从 Dto 映射到 实体
            var model = parm.Adapt<BizBook>().ToUpdate(HttpContext);

            var response = _BizBookService.Update(w => w.Id == model.Id, it => new BizBook()
            {
                //Update 字段映射
                BookName = model.BookName,
                Price = model.Price,
                Author = model.Author,
                Content = model.Content,
            });

            return ToResponse(response);
        }

        /// <summary>
        /// 删除图书管理
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{ids}")]
        [ActionPermissionFilter(Permission = "book:bizbook:delete")]
        [Log(Title = "图书管理", BusinessType = BusinessType.DELETE)]
        public IActionResult DeleteBizBook(string ids)
        {
            int[] idsArr = Tools.SpitIntArrary(ids);
            if (idsArr.Length <= 0) { return ToResponse(ApiResult.Error($"删除失败Id 不能为空")); }

            var response = _BizBookService.Delete(idsArr);

            return ToResponse(response);
        }

        /// <summary>
        /// 导出图书管理
        /// </summary>
        /// <returns></returns>
        [Log(Title = "图书管理", BusinessType = BusinessType.EXPORT, IsSaveResponseData = false)]
        [HttpGet("export")]
        [ActionPermissionFilter(Permission = "book:bizbook:export")]
        public IActionResult Export([FromQuery] BizBookQueryDto parm)
        {
            parm.PageSize = 10000;
            var list = _BizBookService.GetList(parm).Result;

            string sFileName = ExportExcel(list, "BizBook", "图书管理");
            return SUCCESS(new { path = "/export/" + sFileName, fileName = sFileName });
        }

    }
}