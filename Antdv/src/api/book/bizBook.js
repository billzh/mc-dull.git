import request from '@/utils/request'

/**
* 图书管理分页查询
* @param {查询条件} data
*/
export function listBizBook(query) {
  return request({
    url: 'book/BizBook/list',
    method: 'get',
    params: query,
  })
}


/**
* 新增图书管理
* @param data
*/
export function addBizBook(data) {
  return request({
    url: 'book/BizBook',
    method: 'post',
    data: data,
  })
}

/**
* 修改图书管理
* @param data
*/
export function updateBizBook(data) {
  return request({
    url: 'book/BizBook',
    method: 'PUT',
    data: data,
  })
}

/**
* 获取图书管理详情
* @param {Id}
*/
export function getBizBook(id) {
  return request({
    url: 'book/BizBook/' + id,
    method: 'get'
  })
}

/**
* 删除图书管理
* @param {主键} pid
*/
export function delBizBook(pid) {
  return request({
    url: 'book/BizBook/' + pid,
    method: 'delete'
  })
}

// 导出图书管理
export function exportBizBook(query) {
  return request({
    url: 'book/BizBook/export',
    method: 'get',
    params: query
  })
}

